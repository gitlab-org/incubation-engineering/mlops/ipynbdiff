package ipynbdiff

import (
	"bytes"
	"embed"
	"encoding/json"
	"errors"
	"gopkg.in/yaml.v2"
	"regexp"
	"strings"
	"text/template"
)

//go:embed templates/*
var content embed.FS

func formatError(str string) string {
	stripped := regexp.MustCompile("[[0-9][0-9;]*m").ReplaceAllString(str, "")
	var lines []string
	for _, line := range strings.Split(stripped, "\n") {
		line = strings.Replace(line, "\u001B", "    ", 1)
		line = strings.ReplaceAll(line, "\u001B", "")
		lines = append(lines, line)
	}

	return strings.Join(lines, "\n")
}

func Convert(input []byte) (output bytes.Buffer, err error) {
	var notebook Notebook

	if err = json.Unmarshal(input, &notebook); err != nil {
		return
	}

	jupyterConf := &JupyterConf{
		Jupyter: Jupyter{
			Kernelspec:    notebook.Metadata.Kernelspec,
			LanguageInfo:  notebook.Metadata.LanguageInfo,
			Nbformat:      notebook.NbFormat,
			NbformatMinor: notebook.NbFormatMinor,
		},
	}

	t := template.Must(template.New("index.go.tmpl").Funcs(template.FuncMap{
		"dict": func(values ...interface{}) (map[string]interface{}, error) {
			if len(values)%2 != 0 {
				return nil, errors.New("invalid dict call")
			}
			dict := make(map[string]interface{}, len(values)/2)
			for i := 0; i < len(values); i += 2 {
				key, ok := values[i].(string)
				if !ok {
					return nil, errors.New("dict keys must be strings")
				}
				dict[key] = values[i+1]
			}
			return dict, nil
		},
		"formatError": func(value string) (string, error) {
			return formatError(value), nil
		},
		"hasPrefix": func(value string, prefix string) (bool, error) {
			return strings.HasPrefix(value, prefix), nil
		},
	}).ParseFS(content, "templates/*.tmpl"))

	conf, _ := yaml.Marshal(jupyterConf)

	config := map[string]interface{}{
		"JupyterConfYaml": string(conf),
		"Cells":           notebook.Cells,
		"Language":        notebook.Metadata.Kernelspec.Language,
		"EmptySlice":      []string{},
	}

	if err = t.Execute(&output, config); err != nil {
		return
	}

	return
}
