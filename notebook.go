package ipynbdiff

type Notebook struct {
	Cells         []Cell           `json:"cells"`
	Metadata      NotebookMetadata `json:"metadata"`
	NbFormat      int              `json:"nbformat"`
	NbFormatMinor int              `json:"nbformat_minor"`
}

type Cell struct {
	CellType       string       `json:"cell_type"`
	Id             string       `json:"id"`
	Metadata       CellMetadata `json:"metadata"`
	Source         []string     `json:"source"`
	ExecutionCount int32        `json:"execution_count"`
	Outputs        []Output     `json:"outputs"`
}

type Output struct {
	ExecutionCount int32                  `json:"execution_count"`
	OutputType     string                 `json:"output_type"`
	Metadata       map[string]string      `json:"metadata"`
	Data           map[string]interface{} `json:"data"`
	Traceback      []string               `json:"traceback"`
}

type CellMetadata struct {
	Tags []string `json:"tags"`
}

type NotebookMetadata struct {
	Kernelspec   Kernelspec   `json:"kernelspec"`
	LanguageInfo LanguageInfo `json:"language_info"`
}

type Kernelspec struct {
	DisplayName string `json:"display_name" yaml:"display_name"`
	Language    string `json:"language" yaml:"language"`
	Name        string `json:"name" yaml:"name"`
}
type CodemirrorMode struct {
	Name    string `json:"name" yaml:"name"`
	Version int    `json:"version" yaml:"version"`
}
type LanguageInfo struct {
	CodemirrorMode    CodemirrorMode `json:"codemirror_mode" yaml:"codemirror_mode"`
	FileExtension     string         `json:"file_extension" yaml:"file_extension"`
	Mimetype          string         `json:"mimetype" yaml:"mimetype"`
	Name              string         `json:"name" yaml:"name"`
	NbconvertExporter string         `json:"nbconvert_exporter" yaml:"nbconvert_exporter"`
	PygmentsLexer     string         `json:"pygments_lexer" yaml:"pygments_lexer"`
	Version           string         `json:"version" yaml:"version"`
}

type JupyterConf struct {
	Jupyter Jupyter `yaml:"jupyter"`
}

type Jupyter struct {
	Kernelspec    Kernelspec   `yaml:"kernelspec"`
	LanguageInfo  LanguageInfo `yaml:"language_info"`
	Nbformat      int          `yaml:"nbformat"`
	NbformatMinor int          `yaml:"nbformat_minor"`
}
