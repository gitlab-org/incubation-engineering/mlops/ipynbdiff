package ipynbdiff

import (
	"bytes"
	"github.com/pmezard/go-difflib/difflib"
	"os"
)

func PrepareFileForDiff(filePath string, convertToMd bool) (lines []string, err error) {

	if filePath == "/dev/null" {
		return []string{}, nil
	}

	var file []byte
	var diffableFile bytes.Buffer

	if file, err = os.ReadFile(filePath); err != nil {
		return
	}

	if convertToMd {
		if diffableFile, err = Convert(file); err != nil {
			return
		}
	} else {
		diffableFile = *bytes.NewBuffer(file)
	}

	return difflib.SplitLines(diffableFile.String()), err
}

func DiffWithPathAlias(fromPath string, fromPathAlias string, toPath string, toPathAlias string, context int, convertToMd bool) (diff string, err error) {

	var fromLines, toLines []string

	if fromLines, err = PrepareFileForDiff(fromPath, convertToMd); err != nil {
		return
	}

	if toLines, err = PrepareFileForDiff(toPath, convertToMd); err != nil {
		return
	}

	ud := difflib.UnifiedDiff{
		A:        fromLines,
		B:        toLines,
		FromFile: fromPathAlias,
		ToFile:   toPathAlias,
		Context:  context,
	}

	diff, err = difflib.GetUnifiedDiffString(ud)

	return
}

func Diff(fromPath string, toPath string, context int, convertToMd bool) (diff string, err error) {
	return DiffWithPathAlias(fromPath, fromPath, toPath, toPath, context, convertToMd)
}
