package main

import (
	"fmt"
	"gitlab.com/gitlab-org/incubation-engineering/mlops/ipynbdiff"
	"log"
	"os"
)

func main() {

	path := os.Args[1]
	oldFile := os.Args[2]
	oldHex := os.Args[3]
	newFile := os.Args[5]
	newHex := os.Args[6]

	diff, err := ipynbdiff.DiffWithPathAlias(
		oldFile,
		path+"@"+oldHex,
		newFile,
		path+"@"+newHex,
		3,
		true,
	)

	if err != nil {
		log.Fatal(err)
	}

	fmt.Println(diff)

}
